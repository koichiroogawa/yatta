<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('index', function () {
  //  return view("drinks.index",);
//});

/*** 飲み物管理システム ***/
//飲み物関係
Route::get("drinks", "DrinksController@index");
Route::get("drinks/create","DrinksController@create");
Route::get("drinks/{id}","DrinksController@show");
Route::get("drinks/{id}/edit","DrinksController@edit");
Route::post("drinks", "DrinksController@store");
Route::put("drinks/{id}","DrinksController@update");
Route::delete("drinks/{id}","DrinksController@destroy");
// Route::get("logout", "DrinksController@logout");

//メーカー関係
Route::get('maker', 'MakersController@index');
Route::get("maker/create","MakersController@create");
Route::post("maker", "MakersController@store");
Route::put("maker/{id}","MakersController@update");
Route::delete("maker/{id}","MakersController@destroy");
Route::get("maker/{id}/edit","MakersController@edit");
// Route::get('logout', 'MakersController@logout');

//販売履歴関係
Route::get('salelog', 'SalelogController@index');

// じゃんけん課題
Route::get("janken", "JankenController@index");
Route::get('janken/kekka', 'JankenController@kekka');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
