@extends('main')
@include('footer')
@include('header')
@section('title', '飲み物一覧')

	@section('contents')
	<h1>販売ログ</h1>
		<table>
			<tbody>
				@foreach ($salelogs as  $salelog)
				<tr>
					<td>{{$salelog->id}}</td>
					<td>{{$salelog->sale_time}}</td>
					<td>{{$salelog->drink_mst_id}}</td>
				</tr>
 		@endforeach
			</tbody>
		</table>
@endsection
