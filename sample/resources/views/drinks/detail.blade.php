@extends('main')
@include('header')
@include('footer')
@section('title', '飲み物詳細')
@section('contents')

	<h1>飲み物詳細</h1>
	<div class="idback">
		<div class="idstring">[飲み物ID{{$drink->id}}の情報]</div>
	</div>

	<div class="mainback">
		<ul class="shousai">
			<li class="li1">
				<p>商品名</p>
				<div class="shousainame">{{$drink->name}}</div>
			</li>
			
			<li>
				<p>価格</p> 
				@if ($drink->price >= 150) 
				<div class="shousairich">{{$drink->price}}</div>
				@elseif($drink->price >= 101) 
				<div class="shousaimidle">{{$drink->price}}</div>
				@else
				<div class="shousaipoor">{{$drink->price}}</div>
				@endif
			</li>
			<li>
				<p>在庫数</p>
				@if ($drink->stock >= 1000)
				<div class="shousaimany">{{$drink->stock}}</div>
				@elseif ($drink->stock >= 51)
				<div class="shousainormal">{{$drink->stock}}</div> 
				@else
				<div class="shousaifew">{{$drink->stock}}</div>
				@endif
			</li>
		</ul>	
	</div>
	<a href="http://localhost/sample/public/drinks">戻る</a>
@endsection

