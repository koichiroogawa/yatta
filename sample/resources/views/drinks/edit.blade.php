@extends('main')
@include('footer')
@include('header')
@section('title', '飲み物詳細')
@section('css', '飲み物一覧')
@section('contents')<!DOCTYPE html>
	<div class="back">
		<h1>ID{{$drink->id}}を編集</h1>
		<form method="POST" action="/sample/public/drinks/<?php echo $drink->id ?>">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div>
				<span>商品名　：</span>
				<input type="text" name="name" value="{{$drink->name}}">
			</div>
			<div>
				<span>値段　　：</span>
				<input type="text" name="price" value="{{$drink->price}}"></li>
			</div>
			<div>
				<span>在庫数　：</span>
				<input type="text" name="stock" value="{{$drink->stock}}"></li>
			</div>
			<div>
				<span>メーカ名：</span>
				<select name="maker_id">
					@foreach ($makers as $value)
						@if($drink->maker->name == $value->name)
							<option value="{{$value->id}}" selected> {{str_replace("株式会社","(株)",$value->name)}}</option>
						@else
							<option value="{{$value->id}}"> {{str_replace("株式会社","(株)",$value->name)}}</option>
						@endif
					@endforeach
				</select>
			</div>
	 	 	<input type="submit" value="送信">
	 	</form>
	 </div>
	 	 	<a href="/sample/public/drinks">戻る</a>
@endsection
