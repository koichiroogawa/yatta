@extends('main')
@include('header')
@include('footer')
@section('title', '飲み物詳細')
@section('css', '飲み物一覧')
@section('contents')

	<h1>新規登録</h1>
	<div class="back">
		<form method="POST" action="../drinks">
			 {{csrf_field()}}
			 <div>
				商品名　：<input type="text" name = "name">
			</div>
			<div>
				値段　　：<input type="text" name="price"></li>
			</div>
			<div>
				在庫数　：<input type="text" name="stock"></li>
			</div>
			<div>
				メーカ名：<select name="maker_id">
					<?php foreach ($makers as $value) :?>
					<option value="{{$value->id}}"> {{str_replace("株式会社","(株)",$value->name)}}</option>
					<?php endforeach;?>
				</select>
			</div>
		 	 <input type="submit" value="送信">
		 </form>
	</div>
	<a href="/sample/public/drinks">戻る</a>
@endsection