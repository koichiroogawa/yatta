<!DOCTYPE html>
<html lang="ja">
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/test.css') }}">
</head>
<body>
	<h1>飲み物一覧</h1>
	<table>
		<thead>
			<tr>
				<th class="id">id</th>
				<th class ="name">商品名</th>
				<th class="price">価格</th>
				<th class="number">在庫数</th>
				<th class="maker">メーカ名</th>
				<th class="detail">    </th>
			</tr>
		</thead> 
		<tbody>
		@foreach($drinks as $drink)
			<tr>
				<td>{{$drink->id}}</td>
				<td>{{$drink->name}}</td>
				@if ($drink->price >= 150) 
					<td class="rich">{{$drink->price}}</td>
				@elseif ($drink->price >= 101) 
					<td class="midle">{{$drink->price}}</td>
				@else 
					<td class="poor">{{$drink->price}}</td>
				@endif 
				@if ($drink->stock >= 1000)
					<td class="many">{{$drink->stock}}</td>
				@elseif ($drink->stock >= 51)
					<td class="normal">{{$drink->stock}}</td>
				@else
					<td class="few">{{$drink->stock}}</td>
				@endif
				<td>{{str_replace("株式会社","(株)",$drink->mt_name)}}</td>
				<td>
					<a class="rink" href=http://localhost/sample/public/drinks/<?php echo $drink->id ?>>詳細ページ</a>
				</td>
			</tr>
 		@endforeach
		</tbody>
	</table>
</body>
</html>
