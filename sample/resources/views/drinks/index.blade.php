@extends('main')
@include('footer')
@include('header')
@section('title', '飲み物一覧')
@section('contents')
 <div>
	<h1>飲み物一覧</h1>	
</div>
	<div class>
	<table>
		<thead>
			<tr>
				<th class="id d-none d-md-table-cell">				
					@if ($kiriid === "asc")
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortid" value = "desc"  class= "id" type="submit">[id]</button>
					</form>
					@else
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortid" value = "asc"  class= "id" type="submit">[id]</button>
					</form>
					@endif
				</th>
				<th class ="name">
					@if ($kiriname === "asc")
						<form  method="GET"  action="/sample/public/drinks">
							<button  name = "sortname" value = "desc"  class = "name" type="submit">[商品名]</button>
						</form>
					@else
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortname" value = "asc"  class = "name" type="submit">[商品名]</button>
					</form>
					@endif
				</th>
				<th class="price">
					@if ($kiriprice === "asc")
						<form  method="GET"  action="/sample/public/drinks">
							<button  name = "sortprice" value = "desc"  class = "price" type="submit">[価格]</button>
						</form>
					@else
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortprice" value = "asc"  class = "price" type="submit">[価格]</button>
					</form>
					@endif
				</th>
				<th class="number">
					@if ($kiristock === "asc")
						<form  method="GET"  action="/sample/public/drinks">
							<button  name = "sortstock" value = "desc"  class = "number" type="submit">[在庫数]</button>
						</form>
					@else
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortstock" value = "asc"  class = "number" type="submit">[在庫数]</button>
					</form>
					@endif
				</th>
				<th class="maker">
					@if ($kirimaker === "asc")
						<form  method="GET"  action="/sample/public/drinks">
							<button  name = "sortmaker" value = "desc"  class = "maker" type="submit">[メーカー名]</button>
						</form>
					@else
					<form  method="GET"  action="/sample/public/drinks">
						<button name = "sortmaker" value = "asc"  class = "maker" type="submit">[メーカー名]</button>
					</form>
					@endif
				</th>
				<th class="driknkbotton">　　　</th>
				<th class="d-md-table-cell d-none driknkbotton">　　　</th>
				<th class="d-md-table-cell d-none driknkbotton">　　　</th>
			</tr>
		</thead> 
		<tbody>
		@foreach($drinks as $drink)
			<tr>
				<td class="d-md-table-cell d-none">{{$drink->id}}</td>
				<td><a href="drinks/<?php echo $drink->id ?>">{{$drink->name}}</a></td>

				@if ($drink->price >= 150) 
					<td class="rich">{{$drink->price}}</td>
				@elseif ($drink->price >= 101) 
					<td class="midle">{{$drink->price}}</td>
				@else 
					<td class="poor">{{$drink->price}}</td>
				@endif 

				@if ($drink->stock >= 1000)
					<td class="many">{{$drink->stock}}</td>
				@elseif ($drink->stock >= 51)
					<td class="normal">{{$drink->stock}}</td>
				@else
					<td class="few">{{$drink->stock}}</td>
				@endif

				<td>{{str_replace("株式会社","(株)",$drink->maker->name)}}</td>
				<td>
					<a class="btn btn-success driknkbotton" href="drinks/<?php echo $drink->id ?>">詳細</a>
				</td>
				<td class="d-md-table-cell d-none driknkbotton">
					<a class="btn btn-primary" href="drinks/<?php echo $drink->id ?>/edit">編集</a>
				</td>
				<td class="d-md-table-cell d-none driknkbotton">
					<form  method="POST" action="/sample/public/drinks/<?php echo $drink->id ?>">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button class="btn btn-danger" type="submit">
						削除
						</button>
					</form>
				</td>
			</tr>
 		@endforeach
		</tbody>
	</table>
</div>
	<p>
		<a class="rink" href=http://localhost/sample/public/drinks/create>新規登録</a>
	</p>
	<div class="drinksearch">
		<div class="search">商品検索</div>
		<form method = "GET" action="/sample/public/drinks">
			<div class="form-group">
  				<input type="text" name="searchName"  placeholder="[商品名]"  class="form-control text">
  			</div>
  			<div class="form-group text">
				<input type="text" name="searchLowPrice"  placeholder="[最低価格]" class="form-control text">
  			</div>
  			<div class="form-group text">
  				<input type="text" name="searchHighPrice"  placeholder="[最高価格]" class="form-control text">
  			</div>

  		<div class="select">
  			<div class="control-label">
  					選択肢
  			</div>
	  		<div>
	  			<input type="radio" name="searchStock" value="30">30未満
	  		</div>
  			<div>
	  			<input type="radio" name="searchStock" value="50">50未満
	  		</div>
	  		<div>
	  			<input type="radio" name="searchStock" value="100">100未満
	  		</div>
	  		<div>
	  			<input type="radio" name="searchStock" value="300">300未満
	  		</div>
	  		<div>
	  			<input type="radio" value=0 name="searchStock" >
	  				選択なし
	  		</div>
	  	</div>
	  	<div class="form-group">
  			<select name="searchMaker" class="form-control">
  				<option value = 0 selected>該当なし</option>
					@foreach ($makers as $maker)
						<option value="{{$maker->id}}"> {{str_replace("株式会社","(株)",$maker->name)}}</option>
					@endforeach
			</select>
		</div>
  		<input type="submit" value="検索"  class="btn btn-success">
		</form>
	</div>
@endsection
