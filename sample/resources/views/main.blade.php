<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
    
	    <!-- Scripts -->
	    <script src="{{ asset('js/app.js') }}" defer></script>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
		<link rel="stylesheet" type="text/css" href="{{ asset('/css/test.css') }}">
	</head>

	<body>	
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<div id="header">
	@yield('header')
	</div>
	<div id="contents">
	@yield('contents')
	</div>
	<div id="footer">
	@yield('footer')
	</div>
	</body>
</html>
