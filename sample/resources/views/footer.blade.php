@section('footer')
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	<a class="dropdown-item" href="{{ route('logout') }}"
		onclick="event.preventDefault();
		document.getElementById('logout-form').submit();">
		{{ __('Logout') }}
	</a>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		@csrf
	</form>
</div>
<ul class="footer">
	<li><a href="/sample/public/drinks/create">新規登録</a></li>
	<li><a href="/sample/public/drinks/">飲み物リスト</a></li>
	<li><a href="/sample/public/maker/create">メーカー登録</li></a>
	<li><a href="/sample/public/maker">メーカーリスト</li></a>
	<li><a href="/sample/public/salelog">販売ログ</li></a>
</ul>
@endsection('footer')