@section('header')
<ul class="nav nav-pills nav-justified">
	@if (url()->current() == url('drinks/create'))
		<li class="nav-item nav-link active">
			<a href="/sample/public/drinks/create">新規登録</a>
	@else
		<li class="nav-item nav-link">
			<a href="/sample/public/drinks/create">新規登録</a>
	@endif
	</li>
	@if (url()->current() == url('drinks'))
		<li class="nav-item nav-link active">
			<a href="/sample/public/drinks/" >飲み物リスト</a>
	@else
		<li class="nav-item nav-link">
			<a href="/sample/public/drinks/">飲み物リスト</a>
	@endif
	</li>
	@if (url()->current() == url('maker/create'))
		<li class="nav-item nav-link active">
			<a href="/sample/public/maker/create">メーカー登録</a>
	@else
		<li class="nav-item nav-link">
			<a href="/sample/public/maker/create">メーカー登録</a>
	@endif
	</li>
	@if (url()->current() == url('maker'))
		<li class="nav-item nav-link active">
			<a href="/sample/public/maker">メーカーリスト</a>
	@else
		<li class="nav-item nav-link">
			<a href="/sample/public/maker">メーカーリスト</a>
	@endif
	</li>
	@if (url()->current() == url('salelog'))
		<li class="nav-item nav-link active">
			<a href="/sample/public/salelog">販売ログ</a>
	@else
		<li class="nav-item nav-link">
			<a href="/sample/public/salelog">販売ログ</a>
	@endif
	</li>
	<li class="nav-item ">
			<div class="userinfo">
				<span class="userdetail">ユーザーID:{{$userid}}</span>
			</div>
			<div>
				<span class="userdetail">ユーザー:{{$username}}</span>
			</div>
	</li>
	<li class="nav-item ">
		<a href="{{ route('logout') }}"
			onclick="event.preventDefault();
			document.getElementById('logout-form').submit();">
			ログアウト
		</a>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
		</form>
	</li>
</ul>
<div>
</div>
@endsection('header')