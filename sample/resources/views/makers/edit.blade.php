@extends('main')
@include('footer')
@include('header')
@section('title', '飲み物詳細')
@section('css', '飲み物一覧')
@section('contents')<!DOCTYPE html>
	<div class="back">
		<h1>ID{{$maker->id}}を編集</h1>
		<form method="POST" action="/sample/public/maker/<?php echo $maker->id ?>">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div>
				<span>商品名　：</span>
				<input type="text" name="name" value="{{$maker->name}}">
			</div>
	 	 	<input type="submit" value="送信">
	 	</form>
	 </div>
	 	 	<a href="/sample/public/drinks">戻る</a>
@endsection
