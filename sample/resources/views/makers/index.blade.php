@extends('main')
@include('footer')
@include('header')
@section('title', '飲み物一覧')

	@section('contents')
	<h1>メーカー一覧</h1>
	<div>
		<form method = "GET" action="/sample/public/maker">
				<select name="select">
					<option value="id">ID</option>
					<option value="name">メーカー名</option>
  				<input type="text" name="search"  placeholder="[入力してください]">
  			</select>
  	<input type="submit" value="検索">
		</form>
	</div>
	<table>
		<thead>
			<tr>
				<th class="kirikae">
					<form  method="GET"  action="/sample/public/maker">
					@if ($kiriid === "asc")
						<button name = "sortid" value = "desc"  class= "makerid" type="submit">メーカーID</button>
					<!-- </form> -->
					@else
					<!-- <form  method="GET"  action="/sample/public/maker"> -->
						<button name = "sortid" value = "asc"  class= "makerid" type="submit">メーカーID</button>
					@endif
					</form>
				</th>
				<th class="kirikae">
					@if ($kiriname === "asc")
					<form  method="GET"  action="/sample/public/maker">
						<button name = "sortname" value = "desc"  class = "name" type="submit">メーカー名</button>
					</form>
					@else
					<form  method="GET"  action="/sample/public/maker">
						<button name = "sortname" value = "asc"  class = "name" type="submit">メーカー名</button>
					</form>
					@endif
				</th>
				<th>　</th>
				<th></th>
			</tr>
		</thead> 
		<tbody>
				@foreach ($makers as  $maker)
				<tr>
					<td>{{$maker->id}}</td>
					<td>{{str_replace("株式会社","(株)",$maker->name)}}</td>
				<td>
					<a class="btn btn-primary" href="maker/<?php echo $maker->id ?>/edit">編集</a>
				</td>
				<td>
						<form  method="POST" action="/sample/public/maker/<?php echo $maker->id ?>">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button class="btn btn-danger" type="submit">削除</button>
						</form>
				</td>
			</tr>
 		@endforeach
			</tbody>
		</table>
	<div>
		<a class="rink" href="/sample/public/maker/create">新規登録</a>
	</div>
@endsection
