@extends('main')
@include('header')
@include('footer')
@section('title', '飲み物詳細')
@section('css', '飲み物一覧')
@section('contents')
	<h1>メーカー登録</h1>
	<div class="back">
		<form method="POST" action="../maker">
			 {{csrf_field()}}
			 <div>
				メーカー名　：<input type="text" name = "name">
			</div>

		 	 <input type="submit" value="送信">
		 </form>
	</div>
	<a href="/sample/public/maker">戻る</a>
@endsection