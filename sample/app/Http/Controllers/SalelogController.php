<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Sales_log;
use Illuminate\Support\Facades\Auth;

class SalelogController extends Controller {
	 public function __construct() {
        $this->middleware('auth');
    }

	private function getSalelog() {
		$makers = \App\Sales_log::all();

		return $makers;
	}
	// private function getSalelog($id) {
	// 	$maker = \App\Maker::find($id);
	// 	return $maker;
	// }
	public function index() {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
		$ins = \App\Sales_log::query();
		$salelogs = $ins->get();
		return view("sale_logs.index",compact("salelogs","userid","username"));
	}
}
