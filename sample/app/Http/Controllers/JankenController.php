<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class JankenController extends Controller {

	private $gu = "グー";
	private $tyoki = "チョキ";
	private $pa = "パー";

	public function jankenget(){
		$mix = array();
		$mix[1] = $this->gu;
		$mix[2] = $this->tyoki;
		$mix[3] = $this->pa;
		return $mix;
	}
	

	public function judge($myNum, $enemyNum) {
		$resultNum = ($enemyNum - $myNum + 3) % 3;
		if ($resultNum === 0) {
			return "「あいこ」";
		}
		elseif ($resultNum === 1) {
			return "「勝ち」";
		}
		else {
			return "「負け」";
		} 
	}


	public function index() {
		$jankennote = $this->jankenget();
		return view("janken.index", compact("jankennote"));
		exit;
	}


	public function kekka(){
		$jankennote = $this->jankenget();
		$request = request();
		$data = $request->all();
		$enemyNum = rand(1,3);
		$result = $this->judge($data["jibunnote"], $enemyNum);
		$enemy = $jankennote[$enemyNum];
		$zibun = $jankennote[$data["jibunnote"]];


		return view("janken.kekka",compact("result","zibun","enemy"));
		exit;
	}
}
