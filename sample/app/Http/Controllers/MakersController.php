<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Maeker;
use Illuminate\Support\Facades\Auth;

class MakersController extends Controller{
	public function __construct()
    {
        $this->middleware('auth');
    }
	private function getMakers() {
		$makers = \App\Maker::all();

		return $makers;
	}
	private function getMaker($id) {
		$maker = \App\Maker::find($id);
		return $maker;
	}
	public function index(Request $request) {
		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
		$select = $request->input("select");
		if ($select === "id") {
			$searchid = $request->input("search");
		}
		else{
			$searchname = $request->input("search");
			$searchname = str_replace("(株)","株式会社",$searchname);
		}
		
		$ukeid = $request->input("sortid");
		$ukename = $request->input("sortname");
		$kiriid ='asc';
		$kiriname = 'desc';
		$ins = \App\Maker::query();
		if ($ukeid == null && $ukename == null) {
			if (!empty($searchid)) {
				$ins->where("id",$searchid);
			}
			if (!empty($searchname)) {
				$ins->where("name",'like',"%{$searchname}%");
			}
			$ins->orderBy('id',$kiriid);
			$makers = $ins->get();
		}
		elseif (!is_null($ukeid)) {
			$kiriid = $ukeid;
			$ins->orderBy('id',$kiriid);
			$makers=$ins->get();

		}
		else {
			$kiriname = $ukename;
			$ins->orderBy('name',$kiriname);
			$makers = $ins->get();
		}

			return view("makers.index", compact("makers","kiriid","kiriname","userid","username"));
	}
	public function create() {
		 $user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
 		$makers = $this->getMakers();
 		$maker = new \App\Maker();
 		// echo '<pre>';
 		// var_dump($maker);
 		return view("makers.create",compact("makers","userid","username"));
 		exit;
 	}
 	 	public function store(Request $request) {
 		$accept = $request->all();
 		$maker = new \App\Maker();
 		$maker->name = $accept["name"];
 		$maker->save();
 		
 		return redirect('maker');
 		exit;
 		
 	}
 	public function edit(Request $request ,$id) {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
 		$maker = $this->getMaker($id);
 		return view("makers.edit",compact("userid","username","maker"));
 		exit;
 	}
 	public function update(Request $request,$id) {
 		$accept = $request->all();
 		$maker = \App\Maker::find($id);
 		$maker->fill($request->all())->save();
 			return redirect('maker');
 		exit;
 	}
 	 public function destroy(Request $request ,$id){
 		echo $id;
		\App\Maker::destroy($id);
		return redirect('maker');
		exit;
 	}
 	public function logout(){
  		Auth::logout();
  		return redirect('login');
  	}
}