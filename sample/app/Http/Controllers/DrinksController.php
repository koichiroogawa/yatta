<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Drink;
use App\Maeker;
use Illuminate\Support\Facades\Auth;

class DrinksController extends Controller {
 /**
 * 一覧ページ
 *@return IlluminateHttpResponse
 */
 	public function __construct()
    {
        $this->middleware('auth');
    }

	private function getDrinks() {
		$drinks = \App\Drink::query();
		return $drinks;
	}
	public function getDrink($id) {
		$drink = $drinks = \App\Drink::find($id);

		return $drink;
	}

	private function getMakers() {
		$makers = \App\Maker::all();
		return $makers;
	}

 	public function index(Request $request) {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;

 		$ins = \App\Drink::query();
 		$makers = $this->getMakers();
		$ukeid = $request->input("sortid");
		$ukeprice = $request->input("sortprice");
		$ukestock = $request->input("sortstock");
		$ukename = $request->input("sortname");
		$ukemaker = $request->input("sortmaker");
		$searchName = $request->input("searchName");
		$searchLowPrice = $request->input("searchLowPrice");
		$searchHighPrice = $request->input("searchHighPrice");
		$searchStock = $request->input("searchStock");
		$searchMaker = $request->input("searchMaker");
		$kiriid ='asc';
		$kiriname = 'asc';
		$kiriprice = 'asc';
		$kiristock = 'asc';
		$kirimaker = 'asc';

		if ($ukeid == null && $ukename == null && $ukeprice == null && $ukestock == null && $ukemaker == null ) {
			if (!empty($searchName)) {
				$ins->where("name",'like', "%{$searchName}%");
			}
			if (!empty($searchLowPrice)) {
				$ins->where("price",'>=', $searchLowPrice);
			}
			if (!empty($searchHighPrice)) {
				$ins->where("price",'<=',$searchHighPrice);
			}
			if (!empty($searchStock)) {
				$ins->where("stock",'<',$searchStock);
			}
			if (!empty($searchMaker)) {
				$ins->where("maker_id",$searchMaker);
			}
 			$ins->orderBy('id',$kiriid);
 		}
 		elseif (!empty($ukeid)) {
 			$kiriid = $ukeid;
 			$ins->orderBy('id',$kiriid);
 		}
 		elseif (!empty($ukename)) {
 			$kiriname = $ukename;
 			$ins->orderBy('name',$kiriname);
 		}
 		elseif (!empty($ukeprice)) {
 			$kiriprice = $ukeprice;
 			$ins->orderBy('price',$kiriprice);
 		}
 		elseif (!empty($ukestock)) {
 			$kiristock = $ukestock;
 			$ins->orderBy('stock',$kiristock);
 		}
 		elseif (!empty($ukemaker)) {
 			$kirimaker = $ukemaker;
 			$ins->orderBy('maker_id',$kirimaker);
 		}
 		$drinks = $ins->get();
 		return view("drinks.index",compact("drinks","makers","kiriid","kiriname","kiriprice","kiristock","kirimaker","userid","username"));
 		exit;
 	}

 	// 飲み物詳細画面
 	public function show(Request $request ,$id) {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
 		$drink = $this->getDrink($id);
 		return view("drinks.detail",compact("drink", "userid", "username"));
 		exit;
 		var_dump($drink);
 	}

 	public function create() {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
 		$makers = $this->getMakers();
 		return view("drinks.create",compact("makers","userid","username"));
 		exit;
 	}

 	public function store(Request $request) {
 		$accept = $request->all();
 		$drink = new \App\Drink();
 		$drink->fill($request->all())->save();
 		
 			return redirect('drinks');
 		exit;
 	}

 	public function edit(Request $request ,$id) {
 		$user=Auth::user();
 		$userid = Auth::id();
 		$username = $user->name;
 		$drink = $this->getDrink($id);
 		$makers = $this->getMakers();
 		return view("drinks.edit",compact("drink","makers","userid","username"));
 		exit;
 	}
 	public function update(Request $request,$id) {
 		$accept = $request->all();
 		$drink = \App\Drink::find($id);
 		$drink->fill($request->all())->save();
 		return redirect('drinks');
 		exit;
 	}
 	public function destroy(Request $request ,$id){
 		echo $id;
		\App\Drink::destroy($id);
		return redirect('drinks');
		exit;
 	}

 	public function logout(){
  		Auth::logout();
  		return redirect('login');
  	}
}