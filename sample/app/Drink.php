<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
	protected $table = 'drinks';

	public $timestamps = false;
	
	 protected $fillable= array('name','price','stock','maker_id');


	public function maker() {
		return $this->belongsTo('App\Maker');
	}
}