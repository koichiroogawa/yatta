<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Maker extends Model
{
	protected $table = 'makers';

	public $timestamps = false;

	protected $fillable= array('name');

	public function drinks(){
		return $this->hasMany('App\Drink');
	}


	
}