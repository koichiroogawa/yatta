SELECT -- Q1
	COUNT(`id`),`country`
FROM
	`workers`
GROUP BY
	`country`;
    
SELECT -- Q2
	AVG(`age`),`gender`
FROM
	`workers`
GROUP BY
	`gender`;
    
SELECT -- Q3
	`last_name`,`first_name`
FROM
	`workers`
ORDER BY
	`age` DESC,
	`country` ASC;

SELECT -- Q4
	`last_name`AS `苗字` , CASE `country` WHEN 'japan' THEN '日本' WHEN 'United States' THEN 'アメリカ合衆国' ELSE 'それ以外' END AS `国籍`
FROM
	`workers`
;

SELECT -- Q5
	`workers`.`last_name`,`workers`.`first_name`,`jobs`.`name`
FROM
	`workers`
JOIN
	`worker_job_mng`
ON
	`workers`.`id` = `worker_job_mng`.`worker_id` 
JOIN
	`jobs`
ON
	`worker_job_mng`.`job_id` = `jobs`.`id`;
    
SELECT -- Q6
	`workers`.`last_name`AS `苗字`,
	`workers`.`first_name`AS`名前`,
	`workers`.`age`AS`年齢`,
	CASE `workers`.`gender` WHEN '1' THEN '男性' WHEN '2' THEN '女性' END AS `性別`,
	`workers`.`country`AS`国籍`,
	 `jobs`.`name`AS`職業`
FROM
	`workers`
JOIN
	`worker_job_mng`
ON
	`workers`.`id` = `worker_job_mng`.`worker_id` 
JOIN
	`jobs`
ON
	`worker_job_mng`.`job_id` = `jobs`.`id`
ORDER BY
	`jobs`.`id` ASC,
	`workers`.`id` ASC;
