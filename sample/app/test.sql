
SELECT
	COUNT(`id`)
FROM
	`workers`
GROUP BY
	`country`;

SELECT
	AVG(`age`),`gender`
FROM
	`workers`
GROUP BY
	`gender`;

SELECT
	`last_name`,`first_name`
FROM
	`workers`
ORDER BY
	`age` DESC
	`country` DESC;
	

SELECT
	`workers`.`last_name`,`workers`.`first_name`,`jobs`.`name`
FROM
	`workers`
JOIN 
	`worker_job_mng`
ON 
	`workers`.`id` = `worker_job_mng`.`worker_id`
JOIN
	`jobs`
ON
	`worker_job_mng`.`job_id`=`jobs`.`id`


SELECT
	`last_name`,
	CASE WHEN `country` = 'japan' THEN '日本' WHEN `country` = 'United States' THEN 'アメリカ合衆国' END
FROM
	`workers`
ORDER BY
	`age` DESC
	`country` DESC;

SELECT
	`workers`.`last_name` AS `苗字`,`workers`.`first_name`　AS　`名前`,`jobs`.`name`
FROM
	`workers`
JOIN 
	`worker_job_mng`
ON 
	`workers`.`id` = `worker_job_mng`.`worker_id`
JOIN
	`jobs`
ON
	`worker_job_mng`.`job_id`=`jobs`.`id`

